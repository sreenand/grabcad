/**
 * Created by srinand.challur on 8/8/17.
 */
package com.challur;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.event.S3EventNotification.S3EventNotificationRecord;
import com.amazonaws.services.s3.model.AccessControlList;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.ObjectTagging;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.SetObjectTaggingRequest;
import com.amazonaws.services.s3.model.Tag;

public class S3EventProcessor implements
		RequestHandler<S3Event, String> {
	private static final float MAX_WIDTH = 100;
	private static final float MAX_HEIGHT = 100;
	private final String JPG_TYPE = (String) "jpg";
	private final String JPG_MIME = (String) "image/jpeg";
	private final String PNG_TYPE = (String) "png";
	private final String PNG_MIME = (String) "image/png";

	public String handleRequest(S3Event s3event, Context context) {
		try {
			S3EventNotificationRecord record = s3event.getRecords().get(0);
			String srcBucket = record.getS3().getBucket().getName();
			String srcKey = record.getS3().getObject().getKey()
					.replace('+', ' ');
			srcKey = URLDecoder.decode(srcKey, "UTF-8");
			// some comment over here
			AmazonS3Client s3client = new AmazonS3Client(new BasicAWSCredentials("AKIAJ6ICTDF4NJRKKTPQ", "CzyjsbB5tYBJeq+ujZ/hOJWwFGxJCog8bl80d5cZ"));
			AccessControlList acl = s3client.getObjectAcl(srcBucket,srcKey);

			List<Tag> newTags = new ArrayList<Tag>();

			newTags.add(new Tag("State", "InsideEventProcessor"));
			newTags.add(new Tag("State", "InsideEventProcessor2"));

			if(acl.equals(CannedAccessControlList.Private)){
				newTags.add(new Tag("accessTag", "private"));
			} else if(acl.equals(CannedAccessControlList.PublicRead)){
				newTags.add(new Tag("accessTag", "public"));
			} else if(acl.equals(CannedAccessControlList.PublicReadWrite)){
				newTags.add(new Tag("accessTag", "public"));
			}
			s3client.setObjectTagging(new SetObjectTaggingRequest(srcBucket, srcKey, new ObjectTagging(newTags)));
			return "OK";

		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}

