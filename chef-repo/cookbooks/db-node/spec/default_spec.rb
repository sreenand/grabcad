require 'chefspec'
require 'chefspec/berkshelf'



describe 'db-node::default' do
	let :chef_run do
 		ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '14.04').converge(described_recipe)
	end	

	it 'includes the other recipe' do
       expect(chef_run).to include_recipe('include_recipe::mysql')
    end


	# it 'create_user_local' do 
	# 	chef_run.converge(described_recipe)
 #  		expect(chef_run).to run_execute('mysql -uroot -proot -e\"use mysql;CREATE USER \'grabcad_rw\'@\'localhost\' IDENTIFIED BY \'password\'\"')
 #  	end


 #  	it 'create_user_%' do 
 #  		chef_run.converge(described_recipe)
 #  		expect(chef_run).to run_execute('mysql -uroot -proot -e\"use mysql;CREATE USER \'grabcad_rw\'@\'%\' IDENTIFIED BY \'password\'\"')
 #  	end

 #  	it 'grant_access_local' do
 #  		chef_run.converge(described_recipe) 
 #  		expect(chef_run).to run_execute('use mysql;GRANT ALL PRIVILEGES ON * . * TO \'grabcad_rw\'@\'localhost\'')
 #  	end


 #  	it 'grant_access_%' do 
 #  		chef_run.converge(described_recipe)
 #  		expect(chef_run).to run_execute('use mysql;GRANT ALL PRIVILEGES ON * . * TO \'grabcad_rw\'@\'%\'')
 #  	end

end

# describe mysql_config('/etc/mysql-default/my.cnf').params('mysqld') do
#   its('port') { should eq '3306' }
#   its('socket') { should eq '/run/mysql-default/mysqld.sock' }
# end

# describe port 3306 do
#   it { should be_listening }
#   its('protocols') { should include('tcp') }
# end

# describe command("mysql -h 127.0.0.1 -uroot -proot -s -e 'show databases;'") do
#   its('stdout') { should match(/mysql/) }
# end