#
# Cookbook Name:: db-node
# Recipe:: default
#
# Copyright 2017, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

include_recipe "mysql::server"
include_recipe "mysql::client"



mysql_service "default" do
  port '3306'	
  version '5.6'
  initial_root_password "root"
  action [:create, :start]
end


mysql_config 'my.cnf' do
  source 'my.cnf.erb'
  notifies :restart, 'mysql_service[default]'
  action :create
end

mysql_connection_info = {
  host: '127.0.0.1',
  username: 'root',
  password: 'root'
}


mysql_client 'default' do
  action :create
end

mysql_database_user 'grabcad_rw' do
  connection mysql_connection_info
  password passwords['password']
  database_name "grabcad"
  host '%'
  action [:create, :grant]
end


# command "create_user_localhost" do 
#   command "mysql -uroot -proot -e\"use mysql;CREATE USER \'grabcad_rw\'@\'localhost\' IDENTIFIED BY \'password\'\""
#   action :run
# end

# command "create_user_%" do 
#   command "mysql -uroot -proot -e\"use mysql;CREATE USER \'grabcad_rw\'@\'localhost\' IDENTIFIED BY \'password\'\""
#   action :run
# end


# command "grant_access_localhost" do 
#   command "use mysql;GRANT ALL PRIVILEGES ON * . * TO \'grabcad_rw\'@\'localhost\'"
#   action :run
# end

# command "grant_access_%" do 
#   command "use mysql;GRANT ALL PRIVILEGES ON * . * TO \'grabcad_rw\'@\'%\'"
#   action :run
# end