#
# Cookbook Name:: app-node
# Recipe:: default
#
# Copyright 2017, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#


package "apache2" do
  action :install
end


service "apache2" do
  action [:enable, :start]
end

template '/var/www/html/index.html' do
  begin
    source 'index.html.erb'
    owner 'root'
    group 'root'
    mode '0777'  
  rescue Exception => e
    Chef::Log.warn "Unable to perform Copy Index.html"
  end 
  
end


package "php5" do
  action :install
end

package "php-pear" do
  action :install
end

package "php5-mysql" do
  action :install
end

package "php5-mysqlnd" do
  action :install
end


cookbook_file "/etc/php5/apache2/php.ini" do
  source "php.ini"
  mode "0644"
  notifies :restart, "service[apache2]"
end

template "/etc/apache2/mods-enabled/dir.conf" do
  source "dir.conf.erb"
  mode "0644"
  notifies :restart, "service[apache2]"
end


directory "/var/log/php" do
  action :create
  notifies :run, "execute[chownlog]"
end

execute "chownlog" do
  command "chown www-data /var/log/php"
  action :run
end

execute "copy-code" do
  command "cp -r /etc/grabcad/code /var/www/html/"
  action :run
end

execute "restart-apache" do
  command "/etc/init.d/apache2 restart"
  action :run
end


