require 'chefspec'

describe 'app-node::default' do
	let :chef_run do
 		ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '14.04').converge(described_recipe)
	end

	it 'runs a update' do
    	expect(chef_run).to run_execute('apt-get update && apt-get upgrade -y')
  	end

	it "converges successfully" do 
		expect { chef_run }.to_not raise_error
	end


	it "installs correct apache package" do
	    chef_run.converge(described_recipe)
	    expect(chef_run).to install_package('apache2')
	end

	it "enables package after installation" do
	    chef_run.converge(described_recipe)
	    expect(chef_run).to enable_service('apache2')
	end


	it "starts package after installation" do
	    chef_run.converge(described_recipe)
	    expect(chef_run).to start_service('apache2')
	end

	it "installs php5" do 
		chef_run.converge(described_recipe)
	    expect(chef_run).to install_package('php5')
	end

	it "installs php-pear" do 
		chef_run.converge(described_recipe)
	    expect(chef_run).to install_package('php-pear')
	end

	it 'creates a template for index.html' do
    	expect(chef_run).to create_template('/var/www/html/index.html')    	
  	end

  	it 'creates a template for index.html with specified permissions' do
    	expect(chef_run).to create_template('/var/www/html/index.html').with(
	      user: 'root',
	      group: 'root',
	      mode: '0777'	      
	    )   	
  	end

  	it 'creates dir.conf template' do
  		expect(chef_run).to create_template('/etc/apache2/mods-enabled/dir.conf').with(	      
	      mode: '0644'	      
	    )
	end

	it 'creates dir.conf template with required permissions' do
  		expect(chef_run).to create_template('/etc/apache2/mods-enabled/dir.conf')
	end
  	

  	it 'replaces the php.ini' do
  		expect(chef_run).to create_cookbook_file('/etc/php5/apache2/php.ini')    	
  	end

  	it 'creates a cookbook file with specified permissions' do
    	expect(chef_run).to create_cookbook_file('/etc/php5/apache2/php.ini').with(	      
	      mode: '0644'	      
	    )   	
  	end

  	it 'creates a  log directory' do
    	expect(chef_run).to create_directory('/var/log/php')    	
  	end

  	it 'executes chown' do 
  		expect(chef_run).to run_execute('chown www-data /var/log/php')
  	end

  	it 'copies code into sepcified directory' do
  		expect(chef_run).to run_execute('cp -r /etc/grabcad/code /var/www/html/')
  	end


end