#!/bin/bash -v

#configuring Swap
sudo fallocate -l 4G /swapfile
sudo chmod 600 /swapfile
sudo mkswap /swapfile
sudo swapon /swapfile


#installing chef
sudo apt-get update
sudo apt-get install --yes --force-yes git > /tmp/git.log
sudo curl -L https://www.opscode.com/chef/install.sh | bash

sudo add-apt-repository ppa:ondrej/php --yes

cd /etc
sudo git clone https://sreenand@bitbucket.org/sreenand/grabcad.git
sudo chmod -R 777 grabcad

cd /etc/grabcad
git config --global core.editor "vim"

cd /etc/grabcad/chef-repo/cookbooks

echo "cookbook_path ['$PWD']" > solo.rb


sudo chef-solo -c solo.rb -o app-node::default > /tmp/chefinit.log
