#!/bin/bash -v

#configuring Swap
sudo fallocate -l 4G /swapfile
sudo chmod 600 /swapfile
sudo mkswap /swapfile
sudo swapon /swapfile

sudo apt-get update
sudo apt-get install git > /tmp/git.log


apt-add-repository ppa:brightbox/ruby-ng --yes
apt-get update
apt-get install ruby2.2 --yes

sudo apt-get -y install build-essential ruby2.2 git curl build-essential libxml2-dev libxslt-dev libssl-dev autoconf --force-yes
sudo curl -L https://www.opscode.com/chef/install.sh | bash

if [ ! -e /opt/chef/embedded/bin/berks ] ; then
    /opt/chef/embedded/bin/gem install berkshelf --no-ri --no-rdoc
    ln -s /opt/chef/embedded/bin/berks /usr/local/bin/berks
fi


cd /etc
sudo git clone https://sreenand@bitbucket.org/sreenand/grabcad.git
sudo chmod -R 777 grabcad

cd /etc/grabcad
git config --global core.editor "vim"

cd /etc/grabcad/chef-repo/cookbooks

echo "cookbook_path ['$PWD']" > solo.rb


sudo chef-solo -c solo.rb -o mysql::default > /tmp/chefinit.log
