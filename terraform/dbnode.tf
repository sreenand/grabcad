resource "aws_instance" "db-node" {
  ami = "ami-23daa54c"  
  instance_type = "t2.micro"  
  key_name = "${aws_key_pair.deployer.key_name}"
  subnet_id = "${aws_subnet.tf_test_subnet.id}"
  vpc_security_group_ids = ["${aws_security_group.allow_all.id}"]
  user_data       = "${file("install_db_server.sh")}"
  tags {
     Name = "db-node" 
  }
}
